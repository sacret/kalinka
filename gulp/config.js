const build = './build';

const locations = {
  js: './js',
  scss: './scss',
  images: './img'
}

module.exports = {
  locations: locations,
  browserify: {
    files: [
      locations.js + '/routers/Router.js'
    ],
    dest: build,
    name: 'app.js'
  },
  css: {
    files: [
      build + '/css/app.css'
    ],
    name: 'app.css',
    dest: build + '/css/'
  },
  scss: {
    src: locations.scss + '/app.scss',
    watch: locations.scss + '/**',
    dest: build + '/css/'
  },
  images: {
    src: [
      locations.images + '/**'
    ],
    dest: build + '/img'
  },
  fonts: {
    src: [
      './node_modules/bootstrap-sass/assets/fonts/**'
    ],
    dest: build + '/fonts/'
  },
};
