export const header = {
  discount: 'Discount',
  call: 'Позвонить нам'
};

export const main = {
  title: 'Kalinka Group',
  subtitle: 'Недвижимость для счастья!'
};

export const carousel = [{
  key: 1,
  caption: 'Дизайнерские квартиры',
  img: 'build/img/carousel1.png'
}, {
  key: 2,
  caption: 'Дизайнерские квартиры',
  img: 'build/img/carousel1.png'
}, {
  key: 3,
  caption: 'Дизайнерские квартиры',
  img: 'build/img/carousel1.png'
}, {
  key: 4,
  caption: 'Дизайнерские квартиры',
  img: 'build/img/carousel1.png'
}, {
  key: 5,
  caption: 'Дизайнерские квартиры',
  img: 'build/img/carousel1.png'
}];

export const filters = [{
  type: 'action',
  color: 'dark',
  items: [{
    id: 1,
    title: 'Купить'
  }, {
    id: 2,
    title: 'Арендовать'
  }, {
    id: 3,
    title: 'Продать'
  }]
}, {
  type: 'location',
  color: 'khaki',
  items: [{
    id: 1,
    title: 'В Москве'
  }, {
    id: 2,
    title: 'В Санкт-Петербурге'
  }, {
    id: 3,
    title: 'За рубежом'
  }]
}, {
  type: 'purpose',
  color: 'dark',
  items: [{
    id: 1,
    title: 'Жилая'
  }, {
    id: 2,
    title: 'Коммерческая'
  }, {
    id: 3,
    title: 'Загородная'
  }]
}];

export const types = [{
  id: 1,
  title: 'Квартиру'
}, {
  id: 2,
  title: 'Комнату'
}, {
  id: 3,
  title: 'Дом'
}];

export const search = 'Поиск по району, ЖК, названию объекта...';

export const buttons = [{
  id: 1,
  title: 'Больше параметров',
  icon: 'build/img/btn-star.png'
}, {
  id: 2,
  title: 'Найти',
  icon: 'build/img/btn-search.png'
}, {
  id: 3,
  title: 'Заказать подбор объекта',
  icon: 'build/img/btn-check.png'
}, {
  id: 4,
  title: 'Whatsapp',
  icon: 'build/img/btn-whatsapp.png'
}];

export const links = [{
  id: 1,
  title: 'Городская недвижимость',
  icon: 'build/img/links-urban.png'
}, {
  id: 2,
  title: 'Коммерческая недвижимость',
  icon: 'build/img/links-commercial.png'
}, {
  id: 3,
  title: 'Загородная недвижимость',
  icon: 'build/img/links-suburban.png'
}, {
  id: 4,
  title: 'Зарубежная недвижимость',
  icon: 'build/img/links-foreign.png'
}, {
  id: 5,
  title: 'Москва-сити',
  icon: 'build/img/links-moscow.png'
}, {
  id: 6,
  title: 'Партнерская программа',
  icon: 'build/img/links-parnters.png'
}];

export const contacts = {
  title: 'Обратная связь',
  subtitle: 'Будьте в курсе новых предложений:',
  email: 'Электронная почта',
  emailButton: 'Подписаться',
  addresses: [{
    id: 1,
    address: 'Москва, Молочный пер., 1',
    time: 'с 9:30 до 21:30',
    phone: '8 (495) 725 25 81',
    link: 'Карта'
  }, {
    id: 2,
    address: 'Москвовская обл., д. Жукова, Рублево-Успенское ш., стр. 1',
    time: 'с 10:00 до 19:00',
    phone: '8 (495) 725 25 81',
    link: 'Карта'
  }]
}