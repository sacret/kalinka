'use strict';

import React from 'react';
import { render } from 'react-dom';
import { Router, Route, Link, browserHistory } from 'react-router';

import MainPage from '../pages/MainPage';

// render((
//   <Router history={browserHistory}>
//     <Route path="/C:/Users/Sacret/Workspace/kalinka/index.html"      component={MainPage} />
//   </Router>
// ), document.getElementById('content'));

render(
  <MainPage />,
  document.getElementById('content')
);