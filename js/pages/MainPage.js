import React from 'react';
import ReactDOM from 'react-dom';

import Header from '../components/Header';
import Title from '../components/Title';
import CarouselBlock from '../components/CarouselBlock';
import Filters from '../components/Filters';
import Types from '../components/Types';
import Search from '../components/Search';
import Buttons from '../components/Buttons';
import Links from '../components/Links';
import Contacts from '../components/Contacts';

class MainPage extends React.Component {
  render() {
    return (
      <div>
        <Header />
        <Title />
        <CarouselBlock />
        <Filters />
        <Types />
        <Search />
        <Buttons />
        <Links />
        <Contacts />
      </div>
    );
  }
}

export default MainPage;