import React from 'react';
import _ from 'lodash';
import { DropdownButton, MenuItem, Glyphicon } from 'react-bootstrap';

import { types as typesData } from '../data';

class Types extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: typesData[0].title
    };

    this.handleSelect = this.handleSelect.bind(this);
  }

  handleSelect(eventKey, event) {
    this.setState({
      title: typesData[eventKey].title
    });
  }

  render() {
    const dropdownTile = <span className="kalinka-types__title">
      <span>{this.state.title}</span>
      <Glyphicon glyph="chevron-down" className="kalinka-types__chevron"/>
    </span>;

    return (
      <div className="kalinka-types">
        <DropdownButton
          bsStyle="link"
          title={dropdownTile}
          className="kalinka-types__dropdown"
          id="dropdown-types"
          onSelect={this.handleSelect}
        >
          { _.map(typesData, (type, typeIndex) => {
              return (
                <MenuItem
                  eventKey={typeIndex}
                  key={type.id}
                  className="kalinka-types__menu-item"
                >
                  {type.title}
                </MenuItem>
              );
            })
          }
        </DropdownButton>
      </div>
    );
  }
}

export default Types;