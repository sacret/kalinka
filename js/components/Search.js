import React from 'react';
import { FormGroup, FormControl } from 'react-bootstrap';

import { search as searchData } from '../data';

class Search extends React.Component {
  render() {
    return (
      <form className="kalinka-search">
        <FormGroup>
          <FormControl
            type="text"
            placeholder={searchData}
            className="kalinka-search__input"
          />
        </FormGroup>
      </form>
    );
  }
}

export default Search;