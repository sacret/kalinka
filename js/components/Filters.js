import React from 'react';
import _ from 'lodash';
import { ButtonGroup, Button } from 'react-bootstrap';

import { filters as filtersData } from '../data';

class Filters extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      activeItems: [0, 0, 0]
    };
  }

  handleClick(filterDataIndex, itemIndex) {
    this.setState((prevState) => {
      prevState.activeItems.splice(filterDataIndex, 1, itemIndex);
      return ({
        activeItems: prevState.activeItems
      });
    });
  }

  render() {
    return (
      <div className="kalinka-filters">
        { _.map(filtersData, (filterData, filterDataIndex) => {
            const filterColorClass = `kalinka-filters__filter_${filterData.color}`;

            return (
              <ButtonGroup
                key={filterData.type}
                className={`kalinka-filters__filter ${filterColorClass}`}
                justified
              >
                { _.map(filterData.items, (item, itemIndex) => {
                    const isActiveButton = this.state.activeItems[filterDataIndex] == itemIndex;
                    const filterButtonClass = isActiveButton ?
                      `kalinka-filters__button_active` :
                      '';
                    
                    return (
                      <Button
                        key={item.id}
                        href="#"
                        className={`kalinka-filters__button ${filterButtonClass}`}
                        onClick={() => this.handleClick(filterDataIndex, itemIndex)}
                      >
                        {item.title}
                      </Button>
                    );
                  })
                }
              </ButtonGroup>
            );
          })
        }      
      </div>
    );
  }
}

export default Filters;