import React from 'react';
import _ from 'lodash';
import { ButtonGroup, Button, FormGroup, FormControl, InputGroup } from 'react-bootstrap';

import { contacts as contactsData } from '../data';

class Contacts extends React.Component {

  render() {
    return (
      <div className="kalinka-contacts">
        <div className="kalinka-contacts__title">{contactsData.title}</div>
        <div className="kalinka-contacts__subtitle">{contactsData.subtitle}</div>
        <form className="kalinka-contacts__email-form">
          <FormGroup>
            <InputGroup>
              <FormControl
                type="text"
                placeholder={contactsData.email}
                className="kalinka-contacts__input"
              />
              <InputGroup.Button>
                <Button className="kalinka-contacts__button">{contactsData.emailButton}</Button>
              </InputGroup.Button>
            </InputGroup>            
          </FormGroup>
        </form>
        { _.map(contactsData.addresses, (address) => {
            return (
              <div className="kalinka-contacts__address" key={address.id}>
                <div className="kalinka-contacts__address-title">
                  {address.address}
                </div>
                <div className="kalinka-contacts__address-time">
                  {address.time}
                </div>
                <div className="kalinka-contacts__address-phone">
                  {address.phone}
                </div>
                <div className="kalinka-contacts__address-link">
                  {address.link}
                </div>
              </div>
            );
          })
        }      
      </div>
    );
  }
}

export default Contacts;