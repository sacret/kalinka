import React from 'react';
import _ from 'lodash';
import { ButtonGroup, Button } from 'react-bootstrap';

import { buttons as buttonsData } from '../data';

class Buttons extends React.Component {

  render() {
    return (
      <div className="kalinka_buttons">
        <ButtonGroup justified>
          <Button
            key={buttonsData[0].id}
            href="#"
            className="kalinka_buttons__button"
          >
            <div  className="kalinka_buttons__button-container">
              <span className="kalinka_buttons__title">{buttonsData[0].title}</span>
              <img src={buttonsData[0].icon} className="kalinka_buttons__icon"/>
            </div>
          </Button>
          <Button
            key={buttonsData[1].id}
            href="#"
            className="kalinka_buttons__button kalinka_buttons__button_active"
          >
            <div  className="kalinka_buttons__button-container">
              <span className="kalinka_buttons__title">{buttonsData[1].title}</span>
              <img src={buttonsData[1].icon} className="kalinka_buttons__icon" />
            </div>
          </Button>
        </ButtonGroup>

        <ButtonGroup justified>
          <Button
            key={buttonsData[2].id}
            href="#"
            className="kalinka_buttons__button"
          >
            <div  className="kalinka_buttons__button-container">
              <span className="kalinka_buttons__title">{buttonsData[2].title}</span>
              <img src={buttonsData[2].icon} className="kalinka_buttons__icon" />
            </div>
          </Button>
          <Button
            key={buttonsData[3].id}
            href="#"
            className="kalinka_buttons__button"
          >
            <div  className="kalinka_buttons__button-container">
              <span className="kalinka_buttons__title">{buttonsData[3].title}</span>
              <img src={buttonsData[3].icon} className="kalinka_buttons__icon" />
            </div>
          </Button>
        </ButtonGroup>
      </div>
    );
  }
}

export default Buttons;