import React from 'react';

import { main as mainData } from '../data';

class Title extends React.Component {
  render() {
    return (
      <div className="kalinka-title">
        <div className="kalinka-title__title">{mainData.title}</div>
        <div className="kalinka-title__subtitle">{mainData.subtitle}</div>
      </div>
    );
  }
}

export default Title;