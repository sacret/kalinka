import React from 'react';
import _ from 'lodash';
import { Carousel } from 'react-bootstrap';

import { carousel as carouselData } from '../data';

class CarouselBlock extends React.Component {
  render() {
    return (
      <Carousel className="kalinka-carousel" controls={false}>
        { _.map(carouselData, (carouselItem) => {
            return (
              <Carousel.Item key={carouselItem.key}>
                <img className="kalinka-carousel__image" src={carouselItem.img} />
                <Carousel.Caption className="kalinka-carousel__caption">
                  {carouselItem.caption}
                </Carousel.Caption>
              </Carousel.Item>
            );
          })
        }
      </Carousel>
    );
  }
}

export default CarouselBlock;