import React from 'react';
import _ from 'lodash';
import { ButtonGroup, Button } from 'react-bootstrap';

import { links as linksData } from '../data';

class Links extends React.Component {

  render() {
    return (
      <div className="kalinka_links">
        <ButtonGroup justified>
          <Button
            key={linksData[0].id}
            href="#"
            className="kalinka_links__button"
          >
            <div  className="kalinka_links__button-container">
              <img src={linksData[0].icon} className="kalinka_links__icon"/>
              <span className="kalinka_links__title">{linksData[0].title}</span>              
            </div>
          </Button>
          <Button
            key={linksData[1].id}
            href="#"
            className="kalinka_links__button kalinka_links__button_active"
          >
            <div  className="kalinka_links__button-container">
              <img src={linksData[1].icon} className="kalinka_links__icon" />
              <span className="kalinka_links__title">{linksData[1].title}</span>
            </div>
          </Button>
          <Button
            key={linksData[2].id}
            href="#"
            className="kalinka_links__button kalinka_links__button_active"
          >
            <div  className="kalinka_links__button-container">
              <img src={linksData[2].icon} className="kalinka_links__icon" />
              <span className="kalinka_links__title">{linksData[2].title}</span>
            </div>
          </Button>
        </ButtonGroup>

        <ButtonGroup justified>          
          <Button
            key={linksData[3].id}
            href="#"
            className="kalinka_links__button"
          >
            <div  className="kalinka_links__button-container">
              <img src={linksData[3].icon} className="kalinka_links__icon" />
              <span className="kalinka_links__title">{linksData[3].title}</span>
            </div>
          </Button>
          <Button
            key={linksData[4].id}
            href="#"
            className="kalinka_links__button"
          >
            <div  className="kalinka_links__button-container">
              <img src={linksData[4].icon} className="kalinka_links__icon" />
              <span className="kalinka_links__title">{linksData[4].title}</span>
            </div>
          </Button>
          <Button
            key={linksData[5].id}
            href="#"
            className="kalinka_links__button"
          >
            <div  className="kalinka_links__button-container">
              <img src={linksData[5].icon} className="kalinka_links__icon" />
              <span className="kalinka_links__title">{linksData[5].title}</span>
            </div>
          </Button>
        </ButtonGroup>
      </div>
    );
  }
}

export default Links;