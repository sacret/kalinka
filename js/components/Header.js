import React from 'react';
import { Grid, Row, Col } from 'react-bootstrap';

import { header as headerData } from '../data';

class Header extends React.Component {
  render() {
    return (
      <header>
        <Grid fluid className="kalinka-header">
          <Row>
            <Col className="kalinka-header__first-column">
              <Row>
                <Col className="kalinka-header__logo"></Col>
                <Col className="kalinka-header__menu">
                  <div className="kalinka-header__icon-bar-container">
                    <div className="kalinka-header__icon-bar" />
                    <div className="kalinka-header__icon-bar" />
                    <div className="kalinka-header__icon-bar" />
                  </div>
                </Col>
              </Row>
            </Col>
            <Col className="kalinka-header__discount">{headerData.discount}</Col>
            <Col className="kalinka-header__call">{headerData.call}</Col>
          </Row>
        </Grid>
      </header>
    );
  }
}

export default Header;